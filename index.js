/**************************
 * Connection to the host *
 **************************/
const oNet = require('net');
const sHost = "box.pixel-competition.de";
const nPort = 2342;

const client = new oNet.Socket();
client.connect(nPort, sHost, () => {
    console.log(`connected to ${sHost}`);
});

client.on('data', (data) => {
    console.log(String(data));
});

client.on('close', () => {
    console.log('connection closed');
});

// client.write('help\n');

/********************************************
 * draw simple shapes on competition canvas
 * PixelFlut protocol: 
 *  PX {x} {y} {r} {g} {b}
 *     - or -
 *  PX {x} {y} #{color}
 ********************************************/

 // Rectangle
const sColor = '#0000FF';
const nWidth = 400;
const nHeight = 400;

for (let nX = 0; nX < nWidth; nX++) {
    for (let nY = 0; nY < nHeight; nY++) {
        const nOffsetX = nX + 10;
        const nOffsetY = nY + 10;
        const sCmd = `PX ${nOffsetX} ${nOffsetY} ${sColor}`
        console.log(sCmd);
        client.write(sCmd);
    }
}